module.exports = {
  testMatch: ['<rootDir>/test/*test.[jt]s'],
  collectCoverageFrom: ['<rootDir>/src/*.[jt]s'],
  transform: {
    '^.+\\.[jt]s$': 'ts-jest',
  },
  moduleFileExtensions: ['js', 'ts', 'json', 'node'],
  testEnvironment: 'node',
};
